<?php
/*
Template Name: Front Page
*/
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

 <div class="sixteen columns">
   <?php  echo courtyard_slider_home(); ?>
 </div>
 
 <!--BEGIN .tagline options-->
  <div class="sixteen columns tagline">
    <?php if ( function_exists( 'of_get_option' ) ) {
                        echo $tagline = of_get_option( 'tagline' );
                    }
                    ?>
  </div>
  <!--END .tagline options -->

<div class="sixteen columns">
   
  <div class="linebreak-home"></div>
   
  </div>
  
<!--BEGIN featured gallery heading options -->
  <div class="four columns">
    <?php if ( function_exists( 'of_get_option' ) ) {
                        echo $featured_gallery_intro = of_get_option( 'featured_gallery_intro' );
                    }
                    ?>
  </div>
<!--END featured gallery heading options -->
  
  <!--BEGIN three columns query options-->
  <?php
  $num_cols = 3; // set the number of columns here
          
  if ( function_exists( 'of_get_option' ) ) :

     $args = array(
          'post-type' => 'gallery',
          'tax_query' => array(
                  array(
                          'taxonomy' => 'gallery_category',
                          'field' => 'id',
                          'terms' => of_get_option( 'homepage_feature' )
                          
                  )
          )
  );
  $query = new WP_Query( $args );
      
      if ( $query->have_posts() ) :
    for ( $i=1 ; $i <= $num_cols; $i++ ) :
      echo '<div id="col-'.$i.'" class="four columns entry-thumb-home">';
      $counter = $num_cols + 1 - $i;
      while ( $query->have_posts() ) : $query->the_post();
        if( $counter%$num_cols == 0 ) : ?>
	
	<!-- core post area; title, content, thumbnails, postmeta, etc -->
	 <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail-home', array('class' => 'image-fade')); ?></a>
	<p class="entry-title-home"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
	
        <?php endif;
        $counter++;
      endwhile;
      rewind_posts();
      echo '</div>'; //closes the column div
    endfor;
  else:
    echo '<div class="four columns">Please create a post.</div>';
  endif;
  endif; rewind_posts(); wp_reset_postdata();
  ?>
<!--END three columns query options -->

  <div class="sixteen columns">
   
  <div class="linebreak-home"></div>
   
  </div>
  
  
  <!--BEGIN sticky blog heading-->
  <div class="four columns">
    <?php if ( function_exists( 'of_get_option' ) ) {
                        echo $from_the_blog_intro = of_get_option( 'from_the_blog_intro' );
                    }
                    ?>
  </div>
  <!--END sticky blog heading-->
  
  <!--BEGIN sticky blog posts-->
  <?php

          /* Get all sticky posts */
          $sticky = get_option( 'sticky_posts' );

          /* Sort the stickies with the newest ones at the top */
          rsort( $sticky );

          /* Get the 3 newest stickies (change 3 for a different number) */
          $sticky = array_slice( $sticky, 0, 3 );

          /* Query sticky posts */
          query_posts( array( 'post__in' => $sticky, 'ignore_sticky_posts' => 1 ) );


  if (have_posts()) :
    for ( $i=1 ; $i <= $num_cols; $i++ ) :
      echo '<div id="col-'.$i.'" class="four columns entry-thumb-home">';
      $counter = $num_cols + 1 - $i;
      while (have_posts()) : the_post();
        if( $counter%$num_cols == 0 ) : ?>

	<!-- core post area; title, content, thumbnails, postmeta, etc -->
  	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail-home', array('class' => 'image-fade'));  ?></a>
	<p class="entry-title-home"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
        <p class="entry-title-home"><?php the_time('F jS, Y'); ?> ~ <br /><a href="<?php comments_link(); ?>"><?php comments_number( 'no responses', 'one response', '% responses' ); ?></a></p>
	
	<p class="entry-title-home"><?php echo get_the_excerpt(); ?></p>
	
        <?php endif;
        $counter++;
      endwhile;
      rewind_posts();
      echo '</div>'; //closes the column div
    endfor;
  else:
    echo '<div class="four columns">Please create a post.</div>';
  endif;
  wp_reset_query();
  ?>
<!--END sticky blog posts-->
		
</div>
<!-- End Container -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer') ); ?>