<?php
/**
* Shutter functions and definitions
*
* For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
*
* @package 	WordPress
* @subpackage 	Shutter
* @since 	Shutter v0.1
*/

/* ========================================================================================================================

Required external files
	
======================================================================================================================== */

require_once dirname( __FILE__ ) . '/external/starkers-utilities.php';
require_once dirname( __FILE__ ) . '/external/class-tgm-plugin-activation.php';
	
if ( !function_exists( 'optionsframework_init' ) ) {
		define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
		require_once dirname( __FILE__ ) . '/inc/options-framework.php';
	}

/* ========================================================================================================================
	
Theme specific settings
	
======================================================================================================================== */

// Add support for custom backgrounds.
$custom_background_support = array(
	'default-image' => get_template_directory_uri() . '/images/fibers.png',
	);

load_theme_textdomain( 'shutter', get_template_directory() . '/lang' );
	
add_theme_support('custom-background', $custom_background_support );
	
// Add support for post formats, post thumbnails, feeds.
add_theme_support( 'post-formats', array( 'gallery' ) );

add_theme_support( 'post-thumbnails');

// auto set featured image
 
if ( ! function_exists( 'fb_set_featured_image' ) ) {
    
    add_action( 'save_post', 'fb_set_featured_image' );
    function fb_set_featured_image() {
            
            if ( ! isset( $GLOBALS['post']->ID ) )
                return NULL;
                
            if ( has_post_thumbnail( get_the_ID() ) )
                return NULL;
            
            $args = array(
                'numberposts'    => 1,
                'order'          => 'ASC', // DESC for the last image
                'post_mime_type' => 'image',
                'post_parent'    => get_the_ID(),
                'post_status'    => NULL,
                'post_type'      => 'attachment'
            );
            
            $attached_image = get_children( $args );
            if ( $attached_image ) {
                foreach ( $attached_image as $attachment_id => $attachment )
                    set_post_thumbnail( get_the_ID(), $attachment_id );
            }
            
    }
    
}


add_theme_support( 'automatic-feed-links' );
	
// add post-formats to post_type 'my_custom_post_type'
// add post-formats to post_type 'page'
add_post_type_support( 'page', 'post-formats' );
add_post_type_support( 'gallery', 'post-formats' );
	
set_post_thumbnail_size( 600, 400, true );
	
add_image_size( 'thumbnail-home', 210, 175, true ); // for use on the homepage
add_image_size( 'gallery-grid-four', 210, 175, true ); // for use on 4 column gallery pages
add_image_size( 'gallery-grid-three', 280, 218, true ); // for use on 3 column gallery pages
add_image_size( 'gallery-grid-two', 445, 346, true ); // for use on 2 column gallery pages
add_image_size( 'thumbnail-large', 900, 600, true ); // for use on sliders
add_image_size( 'thumbnail-blog', 460, 200, true ); // for use on blog pages
	
// add support for WordPress menu and current menu item css
register_nav_menus(array('primary' => 'Primary Navigation'));
	
	function my_css_attributes_filter($var) {
		return is_array($var) ? array_intersect($var, array('current-menu-item')) : '';
	}
	
// add support for custom excerpt length and more
if ( ! isset( $content_width ) ) $content_width = 900;
	
	function custom_excerpt_length( $length ) {
		return 20;
	}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
	
	function new_excerpt_more( $more ) {
		return '...';
	}
add_filter('excerpt_more', 'new_excerpt_more');
	
	
/* ========================================================================================================================
	
Actions and Filters
	
======================================================================================================================== */

add_action( 'wp_enqueue_scripts', 'script_enqueuer' );
add_action( 'tgmpa_register', 'shutter_register_required_plugins' );

add_filter( 'body_class', 'add_slug_to_body_class' );
add_filter( 'nav_menu_css_class', 'my_css_attributes_filter', 100, 1);
add_filter( 'nav_menu_item_id', 'my_css_attributes_filter', 100, 1);
add_filter( 'page_css_class', 'my_css_attributes_filter', 100, 1);
add_filter( 'use_default_gallery_style', '__return_false' );

//include tgm required plugin	
include( 'lib/tgm_register_plugin.php' );

//include options framework style actions	
include( 'lib/options_framework_actions.php' );

//include custom actions	
include( 'lib/custom_actions.php' );
	
// Register sidebars.
	
add_action( 'widgets_init', 'courtyard_register_sidebars' );
	
function courtyard_register_sidebars() {
		
// Repeat register_sidebar() code for additional sidebars.

// Register the 'primary' sidebar.
	
register_sidebar(
		array(
			'id' => 'footer1',
			'name' => __( 'Footer Widget Left', 'shutter' ),
			'description' => __( 'Left footer widget.', 'shutter' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="widget-title">',
			'after_title' => '</h4>'
		)
	);
	
register_sidebar(
		array(
			'id' => 'footer2',
			'name' => __( 'Footer Widget Middle Left', 'shutter' ),
			'description' => __( 'Middle left footer widget.', 'shutter' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="widget-title">',
			'after_title' => '</h4>'
		)
	);
	
// Repeat register_sidebar() code for additional sidebars.
	
register_sidebar(
		array(
			'id' => 'footer3',
			'name' => __( 'Footer Widget Middle Right', 'shutter' ),
			'description' => __( 'Middle right footer widget.', 'shutter' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="widget-title">',
			'after_title' => '</h4>'
		)
	);
	
// Repeat register_sidebar() code for additional sidebars.
	
register_sidebar(
		array(
			'id' => 'footer4',
			'name' => __( 'Footer Widget Right', 'shutter' ),
			'description' => __( 'Right footer widget.', 'shutter' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4 class="widget-title">',
			'after_title' => '</h4>'
		)
	);
	
}

/* ========================================================================================================================
	
Custom Post Types - include custom post types and taxonimies here e.g.

e.g. require_once( 'custom-post-types/your-custom-post-type.php' );
	
======================================================================================================================== */
	

/* ========================================================================================================================
	
Scripts
	
======================================================================================================================== */

/**
* Add scripts via wp_head()
*
* @return void
* @author Keir Whitaker
*/

function script_enqueuer() {
		
wp_register_script( 'site', get_template_directory_uri().'/js/custom.js', array( 'jquery' ), '', true );
wp_register_script( 'respond', get_template_directory_uri().'/js/respond.min.js', array( 'jquery' ), '', true );
wp_register_script( 'superfish', get_template_directory_uri().'/js/superfish.js', array( 'jquery' ), '', true );
wp_register_script( 'hoverintent', get_template_directory_uri().'/js/hoverIntent.js', array( 'jquery' ), '', true );	
wp_register_script( 'flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array('jquery'), '', true );
wp_register_script( 'selectnav', get_template_directory_uri() . '/js/selectnav.min.js', array('jquery'), '', true );
wp_register_script( 'validate', get_template_directory_uri() . '/js/jquery.validate.min.js', array('jquery'), '', true );
wp_register_script( 'quicksand', get_template_directory_uri() . '/js/jquery.quicksand.js', array('jquery'), '', true );
wp_register_script( 'easing', get_template_directory_uri() . '/js/jquery.easing.1.3.js', array('jquery'), '', true );

wp_register_style( 'screen', get_template_directory_uri().'/style.css', '', '', 'all' );
wp_register_style( 'base', get_template_directory_uri().'/css/base.css', '', '', 'all' );
wp_register_style( 'skeleton', get_template_directory_uri().'/css/skeleton.css', '', '', 'all' );
wp_register_style( 'superfish', get_template_directory_uri().'/css/superfish.css', '', '', 'all' );
wp_register_style( 'flexslider', get_template_directory_uri().'/css/flexslider.css', '', '', 'all' );
		
if ( is_singular() ) wp_enqueue_script( "comment-reply" );
		
wp_enqueue_script( 'site' );
wp_enqueue_script( 'respond' );
wp_enqueue_script( 'superfish' );
wp_enqueue_script( 'hoverintent' );		
wp_enqueue_script( 'flexslider' );
wp_enqueue_script( 'selectnav' );
wp_enqueue_script( 'validate' );
wp_enqueue_script( 'quicksand' );
wp_enqueue_script( 'easing' );

wp_enqueue_style( 'screen' );		
wp_enqueue_style( 'base' );
wp_enqueue_style( 'skeleton' );
wp_enqueue_style( 'superfish' );
wp_enqueue_style( 'flexslider' );
}
	
/* ========================================================================================================================
	
Comments
	
======================================================================================================================== */

/**
* Custom callback for outputting comments 
*
* @return void
* @author Keir Whitaker
*/
	function starkers_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment; 
		?>
		<?php if ( $comment->comment_approved == '1' ): ?>	
		
	
			<article class="linebreak-comments" id="comment-<?php comment_ID() ?>">
			
				<?php echo get_avatar( $comment, 25 ); ?> &nbsp;&nbsp; <?php comment_author_link() ?> // 
				<time><a href="#comment-<?php comment_ID() ?>" pubdate><?php comment_date() ?> at <?php comment_time() ?></a></time>
				
				<div class="line"></div>
				
				<p><?php comment_text() ?></p>
			
			</article>
			
		<?php endif; ?>
		
		<?php 
	}
	
/* ========================================================================================================================
	
Slideshows
	
======================================================================================================================== */

// Initialize Slider
	
function courtyard_slider_start() { ?>
	<script type="text/javascript" charset="utf-8">
		$(window).load(function() {
		  $('#main-slider').flexslider({
		      animation: "fade",
		      controlNav: true,
		      directionNav: true,
		      randomize: true,
		      touch: true
		  });
	      });
	</script>
	<?php }
	
add_action( 'wp_head', 'courtyard_slider_start' );

// Create Slider
	
function courtyard_slider_home() {
 
        // Query Arguments
        $args = array(
            'post_type' => 'slider',
            'posts_per_page' => -1
        );  
 
        // The Query
        $the_query = new WP_Query( $args );
	
        // Check if the Query returns any posts
        if ( $the_query->have_posts() ) {
	
	 // Start the Slider ?>
	 
	<div id="main-slider" class="flexslider">
		<ul class="slides">
		<?php	
                    while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <li>
 
                        <?php // Check if there's a Slide URL given and if so let's a link to it
                        if ( get_post_meta( get_the_id(), 'slide_url', true) != '' ) { ?>
                            <a href="<?php echo esc_url( get_post_meta( get_the_id(), 'slide_url', true) ); ?>"><span class="overlay"></span>
                        <?php }
 
                        // The Slide's Image
                        echo the_post_thumbnail('thumbnail-large', array('class' => 'slider-border'));
 
                        // Close off the Slide's Link if there is one
                        if ( get_post_meta( get_the_id(), 'slide_url', true) != '' ) { ?>
                            </a>
                        <?php } ?>
 
                        </li>
                    <?php endwhile; ?>
		</ul>
	</div>
	<!-- end .flexslider -->
	
	  <?php }
 
        // Reset Post Data
        wp_reset_postdata();
    }
    
    // Slider Shortcode

	function courtyard_slider_shortcode() {
		ob_start();
		courtyard_slider_home();
		$slider = ob_get_clean();
		return $slider;
	}

add_shortcode( 'slideshow_home', 'courtyard_slider_shortcode' );

// Native Gallery Slideshows

if ( !function_exists( 'courtyard_gallery' ) ) {
    function courtyard_gallery($postid, $imagesize) { ?>
        <script type="text/javascript">
    		jQuery(document).ready(function($){
    		 // Slideshow
			 $('.flexslider').flexslider();
			animation: "fade",
		        controlNav: true,
		        directionNav: true,
		        randomize: true,
		        touch: true
 });
		
    	</script>
    <?php 
        $thumbid = 0;
    
        // get the featured image for the post
        if( has_post_thumbnail($postid) ) {
            $thumbid = get_post_thumbnail_id($postid);
        }
        echo "<!-- BEGIN #slider-$postid -->\n<div id='slider-$postid' class='flexslider'>";
        
        $posttitle = the_title_attribute( array( 'echo' => 0 ) );
        
        // get all of the attachments for the post
        $args = array(
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'post_type' => 'attachment',
            'post_parent' => $postid,
            'post_mime_type' => 'image',
            'post_status' => null,
            'numberposts' => -1,
        );
        $attachments = get_posts($args);
        if( !empty($attachments) ) {
            echo '<ul class="slides">';
            $i = 0;
            foreach( $attachments as $attachment ) {
                if( $attachment->ID == $thumbid ) continue;
                $src = wp_get_attachment_image_src( $attachment->ID, $imagesize );
                $caption = $attachment->post_excerpt;
                $caption = ($caption) ? $caption : $posttitle;
                $alt = ( !empty($attachment->post_content) ) ? $attachment->post_content : $attachment->post_title;
                echo "<li><img height='$src[2]' width='$src[1]' src='$src[0]' alt='$alt' title='$caption' class='slider-border' /></li>";
                $i++;
            }
            echo '</ul>';
        }
        echo "<!-- END #slider-$postid -->\n</div>";
    }
}

?>