<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to starkers_comment() which is
 * located in the functions.php file.
 *
 * @package 	WordPress
 * @subpackage 	Shutter
 * @since 	Shutter v0.1
 */
?>

<div id="comments">
	<?php if ( post_password_required() ) : ?>
	<p>This post is password protected. Enter the password to view any comments</p>
</div>

	<?php
			/* Stop the rest of comments.php from being processed,
			 * but don't kill the script entirely -- we still have
			 * to fully load the template.
			 */
			return;
		endif;
	?>


<?php // You can start editing here -- including this comment! ?>

<?php if ( have_comments() ) : ?>

	 <h4 class="comments"><?php comments_number(); ?></h4>

  <ul>
    <?php wp_list_comments( array( 'callback' => 'starkers_comment' ) ); ?>
  </ul><?php paginate_comments_links(); ?><?php
                  /* If there are no comments and comments are closed, let's leave a little note, shall we?
                   * But we don't want the note on pages or post types that do not support comments.
                   */
                  elseif ( ! comments_open() && ! is_page() && post_type_supports( get_post_type(), 'comments' ) ) :
          ?>

  <p>Comments are closed</p><?php endif; ?><?php 
  $comments_args = array(

          'id_form' => 'commentform',
          'id_submit' => 'submit',
          'comment_notes_after' => '',
          'title_reply'=>'Leave a comment',
          'must_log_in' => '<p class="must-log-in">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( get_permalink() ) ) . '</p>'
   
  );


  comment_form($comments_args);
  ?>
  
  </div><!-- #comments -->
