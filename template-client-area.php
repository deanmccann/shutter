<?php
/*
Template Name: Client Area
*/
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<div class="sixteen columns">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

       <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
      <h3 class="page-title"><?php the_title(); ?></h3>

      <div class="horizontal-fade"></div>

      <div class="entry-content clearfix">
        <?php if ( has_post_format( 'gallery' )) {
                            courtyard_gallery($post->ID, 'thumbnail-large');
                        }
                        ?><?php the_content(); ?><?php
                $cat_id = of_get_option( 'client_area' );
                
                $args = array(
                'post-type' => 'password_gallery',
                'posts_per_page' => -1,
                'tax_query' => array(
                        array(
                                'taxonomy' => 'password_gallery_category',
                                'field' => 'id',
                                'terms' => of_get_option( 'client_area' )
                        )
                )
        );
                
                $my_query = null;
                $my_query = new WP_Query($args);
                if( $my_query->have_posts() ) {
        ?>

        <form name="client_gallery" class="clientform" id="client_gallery">
          <select name="menu" class="clientnav">
            <?php
                                      while ($my_query->have_posts()) : $my_query->the_post(); ?>

            <option value="<?php the_permalink() ?>">
              <?php the_title(); ?>
            </option><?php

                                      endwhile;
                                    }
                                    ?>
          </select> <input type="button" onclick=
          "location=document.client_gallery.menu.options[document.client_gallery.menu.selectedIndex].value;" value=
          "View photos" />
        </form>
      </div><?php wp_reset_query();
      ?>
    </div><?php endwhile; ?><?php wp_reset_query(); ?>
  </div>
               
</div>
<!-- End Container -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>