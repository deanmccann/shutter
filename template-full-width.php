<?php
/*
Template Name: Full Width
*/
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

 <div class="container sixteen columns hfeed">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
      <h3 class="page-title"><?php the_title(); ?></h3>

      <div class="horizontal-fade"></div><?php the_content(); ?><?php endwhile; ?><?php wp_reset_query(); ?>
    </div>
  </div>
               
</div>
<!-- End Container -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer') ); ?>
	
	