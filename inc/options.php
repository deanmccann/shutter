<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = get_option( 'stylesheet' );
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'options_framework_theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {
	
	// Defined Stylesheet Paths
	// Use get_template_directory_uri if it's a parent theme
	$defined_stylesheets = array(
		"0" => "Light", // There is no "default" stylesheet to load
		get_template_directory_uri() . '/css/dark.css' => "Dark",
	);
	$options[] = array( "name" => "Select a Stylesheet to load",
		"desc" => "This is a manually defined list of stylesheets.",
		"id" => "stylesheet",
		"std" => "0",
		"type" => "select",
		"options" => $defined_stylesheets );

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		if( isset( $category->cat_ID ) ){
		$options_categories[$category->cat_ID] = $category->cat_name;
		}
	}
	
	// Pull all the custom taxonomies into an array
	$options_taxonomies = array();
	$taxonomies_terms_obj = get_terms('gallery_category');
	foreach ( $taxonomies_terms_obj as $taxonomy) {
		if( isset( $taxonomy->term_id ) ){
		$options_taxonomies[$taxonomy->term_id] = $taxonomy->name;
		}
	}
	
	// Pull all the custom taxonomies into an array
	$options_password_taxonomies = array();
	$taxonomies_password_terms_obj = get_terms('password_gallery_category');
	foreach ( $taxonomies_password_terms_obj as $taxonomy) {
		if( isset( $taxonomy->term_id ) ){
			$options_password_taxonomies[$taxonomy->term_id] = $taxonomy->name;
		}
	}

	$options = array();
	
	$options[] = array(   
		"name" => __("Shutter Theme Settings",'shutter'),
		"desc" => __('<p>Control and configure the general setup of your theme. Upload your preferred logo, customize your homepage and choose between light and dark theme styles.</p>', 'shutter'),
		"type" => "info" );
	
	$options[] = array(   
		"name" => '',
		"desc" => '',
		"type" => "info" );

	// General Settings Section
	$options[] = array(
		'name' => __('General', 'shutter'),
		'type' => 'heading');
	
	// Custom Logo Upload
	$options[] = array(
		'name' => __('Upload Custom Logo', 'shutter'),
		'desc' => __('Upload a logo for your theme.', 'shutter'),
		'id' => 'logo',
		'type' => 'upload');
	
	// Custom Favicon Upload
	$options[] = array(
		'name' => __('Upload Custom Favicon', 'shutter'),
		'desc' => __('Upload a 16px x 16px Png/Gif image that will represent your theme favicon.', 'shutter'),
		'id' => 'favicon',
		'type' => 'upload');
	
	// Select a Featured Homepage Category
	$options[] = array(
		'name' => __('Featured Homepage Category', 'shutter'),
		'desc' => __('Choose a gallery category to feature on your homepage.', 'shutter'),
		'id' => 'homepage_feature',
		'type' => 'select',
		'options' => $options_taxonomies);
	
	// Select a Category for your Client Area
	$options[] = array(
		'name' => __('Password Protected Galleries', 'shutter'),
		'desc' => __('Choose a category for password protected client galleries.', 'shutter'),
		'id' => 'client_area',
		'type' => 'select',
		'options' => $options_password_taxonomies);
	
	// Footer Text
	$options[] = array(
		'name' => __('Enter Footer Text ', 'shutter'),
		'desc' => __('Add some content to your footer. Leave blank to show nothing.', 'shutter'),
		'id' => 'footer_text',
		'std' => '',
		'type' => 'textarea');
	
	// Google Analytics ID
	$options[] = array(
		'name' => __('Add your Google Analytics ID', 'shutter'),
		'desc' => __('Paste your Google Analytics tracking code here. It will be inserted before the closing body tag of your theme.', 'shutter'),
		'id' => 'google_analytics',
		'std' => '',
		'type' => 'textarea');
	
	// Homepage Settings Section
	$options[] = array(
		'name' => __('Homepage Content', 'shutter'),
		'type' => 'heading');
	
	// Text Editor Settings
	$wp_editor_settings = array(
		'wpautop' => true, // Default
		'textarea_rows' => 5,
		'tinymce' => array( 'plugins' => 'wordpress' )
	);
	
	// Add a Tagline
	$options[] = array(
		'name' => __('Add a Tagline', 'shutter'),
		'desc' => __( 'Add an opening tagline to your homepage. Leave blank to show nothing.', 'shutter' ),
		'id' => 'tagline',
		'type' => 'editor',
		'media_buttons' => 'true',
		'settings' => $wp_editor_settings );
	
	// Featured Gallery Intro
	$options[] = array(
		'name' => __('Featured Gallery Heading & Intro', 'shutter'),
		'desc' => __( 'Add a featured gallery heading and intro content to your homepage.', 'shutter'),
		'id' => 'featured_gallery_intro',
		'type' => 'editor',
		'media_buttons' => 'true',
		'settings' => $wp_editor_settings );
	
	// From the Blog Intro
	$options[] = array(
		'name' => __('From the Blog Heading & Intro', 'shutter'),
		'desc' => __( 'Add from the blog heading and intro to your Homepage.', 'shutter'),
		'id' => 'from_the_blog_intro',
		'type' => 'editor',
		'media_buttons' => 'true',
		'settings' => $wp_editor_settings );
	
	// Style & Typography Settings Section
	$options[] = array(
		'name' => __('Style & Typography', 'shutter'),
		'type' => 'heading');
	
	// Select a Stylesheet to be uploaded
	$options[] = array(
		'name' => __('Choose a color scheme.', 'shutter'),
		'desc' => __('Choose a light or dark color scheme for your theme.', 'shutter'),
		'id' => 'stylesheet',
		'type' => 'select',
		"options" => $defined_stylesheets );
	
	$options[] = array(
		'name' => __('Disable Styles','shutter'),
		'desc' => __('Disable option styles and use theme defaults.', 'shutter'),
		'id' => 'disable_styles',
		'std' => true,
		'type' => 'checkbox' );
	
	$typography_mixed_fonts = array_merge( options_typography_get_os_fonts() , options_typography_get_google_fonts() );
	asort($typography_mixed_fonts);
	
	$options[] = array(
		'name' => __('Plain Text Logo Font', 'shutter'),
		'desc' => __('Select font options for your plain text logo.', 'shutter'),
		'id' => 'google_mixed_title',
		'std' => array( 'size' => '48px', 'face' => 'Georgia, serif', 'color' => '#444'),
		'type' => 'typography',
		'options' => array('faces' => $typography_mixed_fonts)
		);
	
	$options[] = array(
		'name' => __('Headings Font', 'shutter'),
		'desc' => __('Choose a font for headings. (h1,h2,h3,h4 etc)', 'shutter'),
		'id' => 'google_mixed_header',
		'std' => array( 'size' => '24px', 'face' => 'Georgia, serif', 'color' => '#444'),
		'type' => 'typography',
		'sizes' => false,
		'options' => array(
			'faces' => $typography_mixed_fonts,
			'styles' => false,
			'sizes' => false)
		);
	
	$options[] = array(
		'name' => __('Body Font', 'shutter'),
		'desc' => __('Select font options for the body text of your theme.', 'shutter'),
		'id' => 'google_mixed_body',
		'std' => array( 'size' => '14px', 'face' => 'Arial, serif', 'color' => '#444'),
		'type' => 'typography',
		'options' => array(
			'faces' => $typography_mixed_fonts)
		);
	
	$options[] = array(
		'name' => __('Primary Navigation Font', 'shutter'),
		'desc' => __('Select font options for the theme navigation menu.', 'shutter'),
		'id' => 'google_mixed_nav',
		'std' => array( 'size' => '16px', 'face' => 'Georgia, serif', 'color' => '#444'),
		'type' => 'typography',
		'options' => array(
			'faces' => $typography_mixed_fonts,
			'sizes' => false)
		);
		
	$options[] = array(
		'name' => __('Link color', 'shutter'),
		"desc" => __("Select the color for links.", 'shutter'),
		"id" => "link_color",
		"std" => "#444",
		"type" => "color" );
		
	$options[] = array(
		'name' => __('Link hover color', 'shutter'),
		"desc" => __('Select the hover color for links.', 'shutter'),
		"id" => "link_hover_color",
		"std" => "#000",
		"type" => "color" );
	
	// About / Support Settings Section
	
	$options[] = array(
		'name' => __('About/Support', 'shutter'),
		'type' => 'heading');
		
	
	$options[] = array(
		'name' => __('Theme Credits', 'shutter'),
		'desc' => __('This theme was handcrafted by Dean McCann at <a href="http://www.courtyardthemes.com">Courtyard Themes</a>.', 'shutter'),
		'type' => 'info');
		
	
	return $options;
}

/*
 * This is an example of how to add custom scripts to the options panel.
 * This example shows/hides an option when a checkbox is clicked.
 */

add_action('optionsframework_custom_scripts', 'optionsframework_custom_scripts');

function optionsframework_custom_scripts() { ?>

<script type="text/javascript">
jQuery(document).ready(function($) {

	$('#example_showhidden').click(function() {
  		$('#section-example_text_hidden').fadeToggle(400);
	});

	if ($('#example_showhidden:checked').val() !== undefined) {
		$('#section-example_text_hidden').show();
	}

});
</script>

<?php
}