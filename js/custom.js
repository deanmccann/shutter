 /*-----------------------------------------------------------------------------------

 	Custom JS - All front-end jQuery
 
-----------------------------------------------------------------------------------*/		

/* SuckerFish --------------------------------------*/
 
 jQuery(document).ready(function($){
        $('ul.sf-menu').superfish({
            delay:         100,
	    speed:         'normal',          	 // speed of the animation. Equivalent to second parameter of jQuery�s .animate() method 
            animation:   {opacity:'show'},  	// fade-in and slide-down animation 
            animationOut:     {opacity:'hide'},
	    autoArrows:  false,                // disable generation of arrow mark-up 
            dropShadows: false   
          
        }); 
    }); 
        
 
/* Quicksand --------------------------------------*/
 
jQuery(document).ready(function($){
		
		//Options for the gallery filter
		var $filterType = $('#sort a').attr('rel');
		var $holder = $('.gallery-content');
		var $data = $holder.clone();
		
		$('#sort a').click(function(e) {
		$('#sort a').removeClass('active');
			var $filterType = $(this).attr('rel');
			$(this).addClass('active');
			
			if ($filterType == 'all') {
				var $filteredData = $data.find('li');
			}
			else {
				var $filteredData = $data.find('li[data-type~=' + $filterType + ']');
			}
			
			$holder.quicksand($filteredData, {
				duration: 800,
				adjustHeight:"auto",
				easing: 'easeInOutQuad',
	
				}, function() {
				
		  	});
		  
		  return false;
		});
	});

 
 /* Flex Slider  --------------------------------------*/

jQuery(document).ready(function($){
       
  jQuery('.flexslider').flexslider({
		      animation: "fade",
		      controlNav: true,
		      directionNav: true,
		      randomize: true,
		      touch: true
       });
  
 });