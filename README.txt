

	SHUTTER

	Created by CourtyardThemes

	- - - - - - - - - - - - - - - - - - - - - - -

	Shutter. A classic, responsive theme for photographers.

	For more details, please visit:
	http://www.courtyardthemes.com

	- - - - - - - - - - - - - - - - - - - - - - -

	Version 1.0.2, 05.12.2013
	
	*Maintenance
		-style.css (version number)
		-gallery-post-type.php (lib/plugins/courtyard-gallery.zip updated ui dashicons for custom post types- lines 41)
		-password-gallery-post-type.php (lib/plugins/courtyard-password-client-gallery.zip updated ui dashicons for custom post types- lines 40)
		-slideshow-post-type.php (lib/plugins/courtyard-slider.zip updated ui dashicons for custom post types- lines 40)



	- - - - - - - - - - - - - - - - - - - - - - -

	Version 1.0.1, 19.11.2013
	
	*Maintenance
		-style.css (version number)
		-style.css (li gallery-item img added border: 5px solid #fff; - lines 412)
	
	- - - - - - - - - - - - - - - - - - - - - - -

	Version 1.0, 04.06.2013
	
	*First Release	

	Enjoy!

	~ Dean McCann


