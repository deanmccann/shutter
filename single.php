<?php
/**
 * The Template for displaying all single posts
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Shutter
 * @since 	Shutter v0.1
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

	<div class="sixteen columns">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <!--BEGIN .hentry -->
   <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
      <h3 class="page-title"><?php the_title(); ?></h3>

      <div class="horizontal-fade"></div>

      <div class="entry-meta clearfix">
        <p>Posted by <?php the_author_posts_link(); ?> on <?php the_date(); ?></p>

        <p>~ <?php the_category('&nbsp; ~ &nbsp;'); ?> ~</p>

        <p><a href="<?php comments_link(); ?>"><?php comments_number( 'no responses', 'one response', '% responses' ); ?></a></p>
      </div>

      <div class="entry-content clearfix">
        <?php if ( has_post_format( 'gallery' )) {
                  courtyard_gallery($post->ID, 'thumbnail-large');
              }
         ?>
         <?php the_content(); ?>
         
         <?php wp_link_pages('before=<p class="pages">' . '&after=</p>'); ?>
         
         <!--BEGIN .blog-navigation .page-navigation -->
        <div class="blog-navigation">
          <div class="pagi-prev">
            <?php previous_post_link('Previous:<br /> %link', '%title'); ?>
          </div>

          <div class="pagi-next">
            <?php next_post_link('Next:<br /> %link', '%title'); ?>
          </div>
        </div>
	<!--END .navigation .page-navigation -->
      </div>

      <div class="linebreak-blog"></div>

      <div class="tags">
        <?php
          if(get_the_tag_list()) {
           echo get_the_tag_list('<ul class="post-tags"><li>','</li><li>','</li></ul>');
         }
        ?>
      </div>
      
      <?php if ( get_the_author_meta( 'description' ) ) : ?><?php echo get_avatar( get_the_author_meta( 'user_email' ) ); ?>

      <h4>About
      <?php echo get_the_author() ; ?></h4><?php the_author_meta( 'description' ); ?>
      <?php endif; ?>
      <?php comments_template( '', true ); ?>
    </div>
  </div>
  <?php endwhile; ?>
  <?php wp_reset_query(); ?>
</div>
<!-- End Container -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>