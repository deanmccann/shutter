<?php
/*
Template Name: Gallery Filterable
*/
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

 <div class="sixteen columns sub-title">
    <h3 class="page-title"><?php the_title(); ?></h3>
    <span class="sub-title-line"></span>
    </div>

    <div class="sixteen columns">&nbsp;</div>
  
  <?php 
                  //get gallery categories
                  $taxonomies = array('gallery_category');
                  
                  $terms = get_terms( $taxonomies );
                  
                  //show filter if categories exist
             if( isset($terms[0]) ) 
                  
                  if($terms[0] !='') { ?>
                  
                  <!-- Portfolio Filter -->

  <div class="sixteen columns content">
    <div class="sort" id="sort">
      <ul class="filter">
        <li><a href="#all" rel="all" class="active"><span><?php _e( 'All', 'shutter' ); ?></span></a></li><?php
                    foreach ($terms as $term ) : ?>

        <li> &nbsp;  <a href="#<?php echo $term->slug; ?>" rel="<?php echo $term->slug; ?>"><span><?php echo $term->name; ?></span></a></li>
        <?php endforeach; ?>
      </ul>
    </div><?php } ?>

    <ul class="gallery-content clearfix">
      <?php
                  //get post type ==> gallery
                 query_posts('post_type=gallery');
                  ?><?php
                          $count=0;
                  while (have_posts()) : the_post();
                              $count++;
                  //get terms
                  $terms = get_the_terms( $post->ID, 'gallery_category' );
                              $terms_list = get_the_term_list( $post->ID, 'gallery_category' );
                  ?><?php if ( has_post_thumbnail() ) {  ?>

      <li data-id="id-<?php echo $count; ?>" data-type="<?php if($terms) { foreach ($terms as $term) { echo $term->slug .' '; } } else { echo 'none'; } ?>" class="gallery-item">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('thumbnail-home'); ?></a>
            <p class="entry-title-home"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
      </li><?php } ?><?php endwhile; ?>
    </ul>
  </div>
  <?php comments_template( '', true ); ?>
  <?php wp_reset_query(); ?>        
</div>
<!-- End Container -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>