<?php
/**
 * The Template for displaying single custom post type gallery
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Shutter
 * @since 	Shutter v0.1
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

 <div class="sixteen columns">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <h3 class="page-title"><?php the_title(); ?></h3>

    <div class="horizontal-fade"></div>

    <div class="entry-content clearfix">
      <?php if ( has_post_format( 'gallery' )) {
              courtyard_gallery($post->ID, 'thumbnail-large');
              }
       ?><?php the_content(); ?>
       
       <?php wp_link_pages('before=<p class="pages">' . '&after=</p>'); ?>
    

       <!--BEGIN .blog-navigation .page-navigation -->
        <div class="blog-navigation">
          <div class="pagi-prev">
            <?php previous_post_link('Previous:<br /> %link', '%title'); ?>
          </div>

          <div class="pagi-next">
            <?php next_post_link('Next:<br /> %link', '%title'); ?>
          </div>
        </div>
	<!--END .navigation .page-navigation -->
   </div>

    
    <?php if ( get_the_author_meta( 'description' ) ) : ?><?php echo get_avatar( get_the_author_meta( 'user_email' ) ); ?>

    <h4>About
    <?php echo get_the_author() ; ?></h4>
    <?php the_author_meta( 'description' ); ?>
      <?php endif; ?>
         <?php comments_template( '', true ); ?>
      <?php endwhile; ?>
   <?php wp_reset_query(); ?>
 </div>
</div>
<!-- End Container -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>