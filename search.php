<?php
/**
 * Search results page
 * 
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Shutter
 * @since 	Shutter v0.1
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

 <div class="sixteen columns hfeed">
    <?php if ( have_posts() ): the_post(); ?>
    <!--BEGIN .hentry -->
    <div <?php post_class() ?> id="post-<?php the_ID(); ?>">

      <h3 class="page-title">Search Results for '<?php echo get_search_query(); ?>'</h3>

      <div class="horizontal-fade">
      </div><?php if ( get_the_author_meta( 'description' ) ) : ?><?php echo get_avatar( get_the_author_meta( 'user_email' ) ); ?>

      <h3>About
      <?php echo get_the_author() ; ?></h3><?php the_author_meta( 'description' ); ?><?php endif; ?><?php rewind_posts(); while ( have_posts() ) : the_post(); ?>

      	<h3 class="entry-blog-title">
	<a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
	</h3>

      <div class="entry-tag-content">
        <?php the_excerpt(); ?>
      </div>

      <div class="entry-meta-blog">
        <p>Posted by <?php the_author_posts_link(); ?> on <?php the_date(); ?> ~
        <?php comments_number( 'no responses', 'one response', '% responses' ); ?></p>
      </div>

      <div class="linebreak-blog"></div><?php endwhile; ?><?php else: ?>

      <h2>No posts to display for <?php echo get_the_author() ; ?></h2><?php endif; ?>
      <!--BEGIN .navigation .page-navigation -->

      <div class="page-navigation">
        <div class="page-prev">
          <?php previous_posts_link('Previous Page','0'); ?>
        </div>

        <div class="page-next">
          <?php next_posts_link('Next Page','0'); ?>
        </div>
      </div>
      <!--END .navigation .page-navigation -->
     </div>
    </div>
  </div>
  <!-- End Container -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>