<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
		
<!-- Basic Page Needs
  ================================================== -->
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="description" content="<?php bloginfo('description'); ?>">
<meta name="author" content="">
<title><?php bloginfo( 'name' ); ?><?php wp_title( '|' ); ?></title>
		
<!-- Mobile Specific Metas
  ================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" user-scalable=0;>
	
<!-- CSS
  ================================================== -->
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
		
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

	
<!-- Favicons
================================================== -->
<?php if ( function_exists( 'of_get_option') ) :  

        if (of_get_option('favicon') != "") : 
 ?>
	<link rel="shortcut icon" href="<?php echo of_get_option('favicon'); ?>"/>
	<?php else:  ?>
	
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico"/>
	
	<?php endif; endif; ?>
		
<!-- Google Analytics
================================================== -->
	
</head>

<body <?php body_class(); ?>>