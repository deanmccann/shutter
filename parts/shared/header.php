<!-- Primary Page Layout
================================================== -->

<!-- Start Container -->
<div id="container-top" class="container hfeed">
    <!-- Begin Header -->

    <div id="logo">
      <?php if ( function_exists( 'of_get_option') ) :  

                if (of_get_option('logo') != "") :
       ?>
                      
          <a href="<?php echo home_url(); ?>"><img src="<?php echo of_get_option('logo'); ?>" /></a>
               
               <?php else:  ?>

    <h2 class="header"><a href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a></h2>
		<h3 class="header"><?php bloginfo('description'); ?></h3>
                
                <?php endif; endif; ?>
    </div>
    <!-- Begin Primary Nav -->
    
    <div class="sixteen columns">
    <div id="primary-nav">
      <?php wp_nav_menu(
                                              array(
                                              'theme_location'        => 'primary',
                                              'menu'                  => 'Primary Navigation',
                                              'container'             => 'false',
                                              'menu_class'            => 'sf-menu',
                                              'menu_id'               => 'nav',
                                              'depth'                 => '0'
                                              )
                                      ); ?>
    </div><!-- End Primary Nav -->
  </div>
    <!--End Header -->