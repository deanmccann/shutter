<!-- Start Container -->
	
<div id="fatfooter">
    <div class="container">
      <div class="four columns">
        <?php dynamic_sidebar( 'footer1' ); ?>
      </div>

      <div class="four columns">
        <?php dynamic_sidebar( 'footer2' ); ?>
      </div>

      <div class="four columns">
        <?php dynamic_sidebar( 'footer3' ); ?>
      </div>

      <div class="four columns">
        <?php dynamic_sidebar( 'footer4' ); ?>
      </div>
    </div><!-- End Container -->

    <div class="sixteen columns">
	<footer id="footer">
	<div class="container">
	<?php if ( function_exists( 'of_get_option' ) ) {
          echo $footer = of_get_option( 'footer_text' );
         }
         ?>	
	</div>
      </footer>
    </div>
  </div>