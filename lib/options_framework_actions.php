<?php

/*
 * The CSS file selected in the options panel 'stylesheet' option
 * is loaded on the theme front end
 */
	 
function options_stylesheets_alt_style()   {
if ( of_get_option('stylesheet') ) {
			wp_enqueue_style( 'options_stylesheets_alt_style', of_get_option('stylesheet'), array(), null );
		}
	}
	
add_action( 'wp_enqueue_scripts', 'options_stylesheets_alt_style' );


/**
 * Returns an array of system fonts
 * Feel free to edit this, update the font fallbacks, etc.
 */

function options_typography_get_os_fonts() {
	// OS Font Defaults
	$os_faces = array(
		'Arial, sans-serif' => 'Arial',
		'Garamond, "Hoefler Text", Times New Roman, Times, serif' => 'Garamond',
		'Georgia, serif' => 'Georgia',
		'"Helvetica Neue", Helvetica, sans-serif' => 'Helvetica Neue',
		'Tahoma, Geneva, sans-serif' => 'Tahoma',
	);
	return $os_faces;
}

/**
 * Returns a select list of Google fonts
 * Feel free to edit this, update the fallbacks, etc.
 */

function options_typography_get_google_fonts() {
	// Google Font Defaults
	$google_faces = array(
		'Abril Fatface, cursive' => 'Abril Fatface',
		'Arvo, serif' => 'Arvo',
		'Crimson Text, serif' => 'Crimson',
		'Droid Sans, sans-serif' => 'Droid Sans',
		'EB Garamond, serif' => 'EB Garamond',
		'Lato, serif' => 'Lato',
		'Lobster, cursive' => 'Lobster',
		'Josefin Slab, serif' => 'Josefin Slab',
		'Open Sans, sans-serif' => 'Open Sans',
		'Old Standard TT, serif' => 'Old Standard TT',
		'Pacifico, cursive' => 'Pacifico',
		'Rokkitt, serif' => 'Rokkit',
		'Raleway, cursive' => 'Raleway',
		'Ubuntu, sans-serif' => 'Ubuntu',
		'Vollkorn, serif' => 'Vollkorn',
	);
	return $google_faces;
}

/* 
 * Outputs the selected option panel styles inline into the <head>
 */
 
function options_typography_styles() {
 	
 	// It's helpful to include an option to disable styles.  If this is selected
 	// no inline styles will be outputted into the <head>
 	
 		if ( !of_get_option( 'disable_styles' ) ) {
		$output = '';
		$input = '';
		
		if ( of_get_option( 'google_mixed_title' ) ) {
			$output .= options_typography_font_styles( of_get_option( 'google_mixed_title' ) , '.header');
		}
		
		if ( of_get_option( 'google_mixed_header' ) ) {
			$output .= options_typography_font_styles( of_get_option( 'google_mixed_header' ) , 'h1,h2,h3,h4,h5,h6, blockquote, blockquote p');
		}
		
		if ( of_get_option( 'google_mixed_body' ) ) {
			$output .= options_typography_font_styles( of_get_option( 'google_mixed_body' ) , 'body, li.cat-item, ul.filter a, ul.filter a:hover, ul.filter a.active, .button, button, input[type="submit"], input[type="reset"], input[type="button"]');
		}
		
		if ( of_get_option( 'google_mixed_nav' ) ) {
			$output .= options_typography_font_styles( of_get_option( 'google_mixed_nav' ) , '.sf-menu, .sf-menu a, #fatfooter a');
		}
		
		if ( of_get_option( 'link_color' ) ) {
			$output .= 'a, a:visited {color:' . of_get_option( 'link_color' ) . '}';
		}
		
		if ( of_get_option( 'link_hover_color' ) ) {
			$output .= 'a:hover, .sf-menu a:hover, #fatfooter a:hover {color:' . of_get_option( 'link_hover_color' ) . '}';
		}
		
		
		if ( $output != '' ) {
			$output = "\n<style>\n" . $output . "</style>\n";
			echo $output;
		}
	}
}
add_action('wp_head', 'options_typography_styles');

/* 
 * Returns a typography option in a format that can be outputted as inline CSS
 */
 
function options_typography_font_styles($option, $selectors) {
		$output = $selectors . ' {';
		$output .= ' color:' . $option['color'] .'; ';
		$output .= 'font-family:' . $option['face'] . '; ';
		$output .= 'font-weight:' . $option['style'] . '; ';
		$output .= 'font-size:' . $option['size'] . '; ';
		$output .= '}';
		$output .= "\n";
		return $output;
}

/**
 * Checks font options to see if a Google font is selected.
 * If so, options_typography_enqueue_google_font is called to enqueue the font.
 * Ensures that each Google font is only enqueued once.
 */
 
if ( !function_exists( 'options_typography_google_fonts' ) ) {
	function options_typography_google_fonts() {
		$all_google_fonts = array_keys( options_typography_get_google_fonts() );
		// Define all the options that possibly have a unique Google font
                $google_font = of_get_option('google_mixed_title', false);
		$google_mixed = of_get_option('google_mixed_header', false);
		$google_mixed_2 = of_get_option('google_mixed_body', false);
		$google_mixed_3 = of_get_option('google_mixed_nav', false);
		// Get the font face for each option and put it in an array
		$selected_fonts = array(
			$google_font['face'],
			$google_mixed['face'],
			$google_mixed_2['face'],
			$google_mixed_3['face'] );
		// Remove any duplicates in the list
		$selected_fonts = array_unique($selected_fonts);
		// Check each of the unique fonts against the defined Google fonts
		// If it is a Google font, go ahead and call the function to enqueue it
		foreach ( $selected_fonts as $font ) {
			if ( in_array( $font, $all_google_fonts ) ) {
				options_typography_enqueue_google_font($font);
			}
		}
	}
}

add_action( 'wp_enqueue_scripts', 'options_typography_google_fonts' );

/**
 * Enqueues the Google $font that is passed
 */
 
function options_typography_enqueue_google_font($font) {
	$font = explode(',', $font);
	$font = $font[0];
	// Certain Google fonts need slight tweaks in order to load properly
	// Like our friend "Raleway"
	if ( $font == 'Raleway' )
		$font = 'Raleway:100';
	$font = str_replace(" ", "+", $font);
	wp_enqueue_style( "options_typography_$font", "http://fonts.googleapis.com/css?family=$font", false, null, 'all' );
}
?>