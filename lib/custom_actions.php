<?php

// enable threaded comments
function enable_threaded_comments(){
	if (!is_admin()) {
		if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1))
			wp_enqueue_script('comment-reply');
		}
}
add_action('get_header', 'enable_threaded_comments');
	
// trigger responsive nav script

function __responsive_script() { ?> 
	<script>
	selectnav('nav');
	</script>
	<?php }
	
add_action( 'wp_footer', '__responsive_script', 100 );

// add google analytics support
	
function __analytics_head()
	{
	if (of_get_option('google_analytics') != "") : 
	    ?>
		<script type="text/javascript">
		    var _gaq = _gaq || [];
		    _gaq.push(['_setAccount', '<?php echo esc_js( of_get_option('google_analytics') ) ; ?>']);
		    _gaq.push(['_trackPageview']);
		    (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		    })();
		</script>
	    
	    <?php endif;
	}
	
add_action( 'wp_head', '__analytics_head', 100 );

	
// add media library re-attachment support

add_filter("manage_upload_columns", 'upload_columns');
add_action("manage_media_custom_column", 'media_custom_columns', 0, 2);

	function upload_columns($columns) {

			unset($columns['parent']);
			$columns['better_parent'] = "Parent";

		return $columns;

}

function media_custom_columns($column_name, $id) {

	$post = get_post($id);

if($column_name != 'better_parent')
		return;

	if ( $post->post_parent > 0 ) {
		if ( get_post($post->post_parent) ) {
				$title =_draft_or_post_title($post->post_parent);
			}
			?>
			<strong><a href="<?php echo get_edit_post_link( $post->post_parent ); ?>"><?php echo $title ?></a></strong>, <?php echo get_the_time(__('Y/m/d', 'shutter')); ?>
			<br />
			<a class="hide-if-no-js" onclick="findPosts.open('media[]','<?php echo $post->ID ?>');return false;" href="#the-list"><?php _e('Re-Attach', 'shutter'); ?></a>

			<?php
		} else {
			?>
			<?php _e('(Unattached)', 'shutter'); ?><br />
			<a class="hide-if-no-js" onclick="findPosts.open('media[]','<?php echo $post->ID ?>');return false;" href="#the-list"><?php _e('Attach', 'shutter'); ?></a>
			<?php
		}

	}
	
// customise password protected page
function my_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<div align="center"><form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
    ' . __( "To view this gallery, please enter your password below:<br /><br />" ) . '
    <label for="' . $label . '">' . __( "Password:" ) . ' </label><input name="post_password" id="' . $label . '" type="password" size="20" /><input type="submit" name="Submit" value="' . esc_attr__( "Submit" ) . '" />
    </form></div>
    ';
    return $o;
}
add_filter( 'the_password_form', 'my_password_form' );

// remove junk from head
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

?>