<?php
/**
 * The Template for displaying single custom post type client gallery posts
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Shutter
 * @since 	Shutter v0.1
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<div class="sixteen columns">
  <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

  <h3 class="page-title"><?php the_title(); ?></h3>

  <div class="horizontal-fade"></div>

  <div class="entry-content clearfix">
    <?php if ( has_post_format( 'gallery' )) {
            courtyard_gallery($post->ID, 'thumbnail-large');
            }
     ?><?php the_content(); ?><?php wp_link_pages('before=<p class="pages">' . '&after=</p>'); ?>
  </div>
  <?php endwhile; ?>
  <?php wp_reset_query(); ?>

</div>

</div>
<!-- End Container -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>