<?php
/*
Template Name: Archives
*/
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

 <div class="sixteen columns hfeed">
    <!--BEGIN .hentry -->
   <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
      <h3 class="page-title"><?php the_title(); ?></h3>

      <div class="horizontal-fade"></div>
    </div>
  </div><?php the_post(); the_content();  ?>

  <div class="five columns">
    <h3>Last 30 Posts</h3><?php wp_get_archives( array( 'type' => 'postbypost', 'limit' => 30, 'format' => 'custom', 'before' => '<li class="cat-item">', 'after' => '</li>' ) ); ?>
  </div>

  <div class="five columns">
    <h3>Archives by
    Month</h3><?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 12, 'format' => 'custom', 'before' => '<li class="cat-item">', 'after' => '</li>' ) ); ?>
  </div>

  <div class="five columns">
    <h3>Archives by Subject</h3><?php wp_list_categories( array( 'style' => 'list', 'title_li' => '' ) ); ?>
  </div>	
</div>
<!-- End Container -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>