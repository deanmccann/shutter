<?php
/*
Template Name: Gallery Two Columns
*/
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

 <div class="sixteen columns">
  <!--BEGIN .hentry -->
   <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
      <h3 class="page-title"><?php the_title(); ?></h3>
      <div class="horizontal-fade"></div>
  </div>
</div>
  
  <!--BEGIN 2 column output -->
  <?php

  $num_cols = 2; // set the number of columns here

  $temp = $wp_query; 
    $wp_query = null; 
    $wp_query = new WP_Query(); 
    $wp_query->query('post_type=gallery'.'&paged='.$paged);

    for ( $i=1 ; $i <= $num_cols; $i++ ) :
      echo '<div id="col-'.$i.'" class="eight columns entry-thumb">';
      $counter = $num_cols + 1 - $i;
      while ($wp_query->have_posts()) : $wp_query->the_post(); 
        if( $counter%$num_cols == 0 ) : ?><!-- core post area; title, content, thumbnails, postmeta, etc -->
  <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('gallery-grid-two', array('class' => 'image-fade')); ?></a>
	<h4 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
        <?php endif;
        $counter++;
      endwhile;
      echo '</div>'; //closes the column div
    endfor;
  ?>

  <div class="sixteen columns">
    <div class="linebreak-blog"></div>
    <!--BEGIN .page navigation -->
    <div class="page-navigation">
      <div class="page-prev">
        <?php previous_posts_link('Previous Page','0'); ?>
      </div>

      <div class="page-next">
        <?php next_posts_link('Next Page','0'); ?>
      </div>
    </div><!--END .page-navigation -->
  </div>

</div><!-- End Container -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer') ); ?>