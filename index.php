<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file 
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Shutter
 * @since 	Shutter v0.1
 */
?>

<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<div class="sixteen columns">
    <h3 class="page-title"><?php wp_title(''); ?></h3>

    <div class="horizontal-fade"></div>
  </div><?php
  $num_cols = 2; // set the number of columns here
  //the query section is only neccessary if the code is used in a page template//


    for ( $i=1 ; $i <= $num_cols; $i++ ) :
      echo '<div id="col-'.$i.'" class="eight columns entry-thumb">';
      $counter = $num_cols + 1 - $i;
      while (have_posts()) : the_post();
        if( $counter%$num_cols == 0 ) : ?>
        <!-- core post area; title, content, thumbnails, postmeta, etc -->
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail-blog', array('class' => 'image-fade')); ?></a>
	<h4 class="entry-blog-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

  <div class="entry-content clearfix">
    <p><?php the_excerpt(); ?></p>

    <p>Posted by <?php the_author_posts_link(); ?> on <?php the_time('F jS, Y'); ?></p>
  </div>

  <div class="linebreak-cats"></div><?php endif;
        $counter++;
      endwhile;
      rewind_posts();
      echo '</div>'; //closes the column div
    endfor;
  wp_reset_query();
  ?><!--BEGIN .navigation .page-navigation -->

  <div class="blog-navigation">
    <div class="pagi-prev">
      <?php previous_posts_link('%link',''); ?>
    </div>

    <div class="pagi-next">
      <?php next_posts_link('%link',''); ?>
    </div>
  </div>
  <!--END .navigation .page-navigation -->
</div>
<!-- End Container -->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer') ); ?>